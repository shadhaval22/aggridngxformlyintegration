﻿using AgGrid.Attributes;
using App.Models.Dtos;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Ngx.Core.Helpers;
using Server.API.Helpers;
using System.Collections.Generic;
using System.Reflection;

namespace Server.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DefinationController : ControllerBase
    {
        [HttpGet]
        [Route("getUserGridColumnDefination")]
        public IActionResult GetUserGridColumnDefination()
        {
            string json = SerializationHelpers
                         .SerializeAppliedPropertyAttributes<GridColumnDefination>(typeof(UserDTO));
            return Ok(json);
        }

        [HttpGet]
        [Route("getUserFormDefination")]
        public IActionResult GetUserFormDefination()
        {
            var formFieldConfigurations= NgxFormHelper.GetFormFieldDefination(typeof(UserDTO));
            return Ok(formFieldConfigurations);
        }
    }
}
