﻿using App.Data;
using App.Models.Dtos;
using App.Models.Entities;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Server.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly ApplicationDataContext _db;
        public UsersController(ApplicationDataContext db,IMapper mapper)
        {
            this._db= db;
            this._mapper=mapper;
        }

        // GET: api/<UsersController>
        [HttpGet]
        [Route("GetUserDetails")]
        public IEnumerable<UserDTO> Get()
        {
            var users = _db.Users.ToList();
            var userDtoList=new List<UserDTO>();
            foreach (var user in users)
            {
                userDtoList.Add(new UserDTO()
                {
                    UserName = user.UserName,
                    EmailId = user.EmailId,
                    MobileNumber = user.MobileNumber,
                    Gender = user.Gender,
                    Pincode = user.PinCode
                });
            }
            return userDtoList;
        }

        // GET api/<UsersController>/5
        [HttpGet]
        [Route("GetUsersDetailsById/{userId}")]
        public UserDTO GetUsersById(int userId)
        {
           var user=_db.Users.FirstOrDefault(x=>x.Id==userId);
           if (user == null)
           {
                throw new Exception("Cannot Find the Associated User");
           }
           var userDTO=new UserDTO();
           _mapper.Map(user, userDTO);
            return userDTO;
        }

        // POST api/<UsersController>
        [HttpPost]
        [Route("InsertNewUser")]
        public IActionResult Post([FromBody] UserDTO userDTO)
        {
            try
            {
                if (userDTO == null)
                {
                    return BadRequest("Bad Data");
                }
                var user = new User()
                {
                    EmailId = userDTO.EmailId,
                    Gender = userDTO.Gender,
                    MobileNumber = userDTO.MobileNumber,
                    PinCode = userDTO.Pincode,
                    UserName = userDTO.UserName,
                };
                _db.Users.Add(user);
                _db.SaveChanges();
                return Ok(user);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // PUT api/<UsersController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<UsersController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
