﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Reflection;

namespace Server.API.Helpers
{
    public class SuppressAttributeTypeIdResolver : CamelCasePropertyNamesContractResolver
    {
        protected override JsonProperty CreateProperty(MemberInfo member, MemberSerialization memberSerialization)
        {
            JsonProperty prop = base.CreateProperty(member, memberSerialization);
            if (member.DeclaringType == typeof(Attribute) && member.Name == "TypeId")
            {
                prop.ShouldSerialize = obj => false;
            }
            return prop;
        }
    }
}
