﻿using Newtonsoft.Json;
using System;
using System.Linq;
using System.Reflection;

namespace Server.API.Helpers
{
    public class SerializationHelpers
    {
        public static string SerializeAppliedPropertyAttributes<T>(Type targetClass) where T : Attribute
        {
            var attributes = targetClass.GetProperties()
                            .SelectMany(p => p.GetCustomAttributes<T>())
                            .ToList();

            JsonSerializerSettings jsonSerializerSettings = new JsonSerializerSettings()
            {
                NullValueHandling = NullValueHandling.Ignore,
                ContractResolver = new SuppressAttributeTypeIdResolver(),
                Formatting = Formatting.Indented,
            };

            return JsonConvert.SerializeObject(attributes, jsonSerializerSettings);
        }
    }
}
