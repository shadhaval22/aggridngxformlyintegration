﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgGrid.Attributes
{
    public class GridColumnDefination :Attribute
    {
        public string HeaderName { get; set; }
        public string Field { get; set; }
        public bool EnableSorting { get; set; }
        public bool Editable { get; set; }
        public bool Sortable { get; set; }
    }
}
