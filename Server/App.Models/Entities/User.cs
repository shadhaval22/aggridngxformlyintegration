﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Models.Entities
{
    public class User
    {
        public int Id { get; set; }

        public string UserName { get; set; }

        public string EmailId { get; set; }

        public char Gender { get; set; }

        public string MobileNumber { get; set; }

        public string PinCode { get; set; }
    }
}
