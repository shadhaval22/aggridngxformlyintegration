﻿using AgGrid.Attributes;
using NgxFormly.Core.Attributes;

namespace App.Models.Dtos
{
    public class UserDTO
    {
       
        [GridColumnDefination(
            HeaderName ="UserName",
            Field ="userName",
            Editable =true,
            EnableSorting =true,
            Sortable =true)]
        [FormFieldDefaultDefination(Key ="userName",Type ="input")]
        [FormWrapperDefination(Wrappers = "form-field-horizontal")]
        [FormTemplateDefination(Label ="UserName:",Type ="text",PlaceHolder ="Enter UserName",Required =true)]
        public string UserName { get; set; }

        [GridColumnDefination(
          HeaderName = "Email Id",
          Field = "emailId",
          Editable = true,
          EnableSorting = true,
          Sortable = true)]

        [FormFieldDefaultDefination(Key = "emailId", Type = "input")]
        [FormWrapperDefination(Wrappers = "form-field-horizontal")]
        [FormTemplateDefination(Label = "Email Id:", Type = "text", PlaceHolder = "Enter Email Id", Required = true)]
        public string EmailId { get; set; }

        [GridColumnDefination(
         HeaderName = "Gender",
         Field = "gender",
         Editable = true,
         EnableSorting = true,
         Sortable = true)]

        [FormFieldDefaultDefination(Key = "gender", Type = "input")]
        [FormWrapperDefination(Wrappers = "form-field-horizontal")]
        [FormTemplateDefination(Label = "Gender:", Type = "text", PlaceHolder = "Enter Gender", Required = true)]
        public char Gender { get; set; }

        [GridColumnDefination(
        HeaderName = "Gender",
        Field = "gender",
        Editable = true,
        EnableSorting = true,
        Sortable = true)]

        [FormFieldDefaultDefination(Key = "mobileNumber", Type = "input")]
        [FormWrapperDefination(Wrappers = "form-field-horizontal")]
        [FormTemplateDefination(Label = "Mobile Number:", Type = "text", PlaceHolder = "Enter Mobile Number", Required = true)]
        public string MobileNumber { get; set; }

        [GridColumnDefination(
       HeaderName = "PinCode",
       Field = "pincode",
       Editable = true,
       EnableSorting = true,
       Sortable = true)]

        [FormFieldDefaultDefination(Key = "pincode", Type = "input")]
        [FormWrapperDefination(Wrappers = "form-field-horizontal")]
        [FormTemplateDefination(Label = "Pin Code:", Type = "text", PlaceHolder = "Enter PinCode", Required = true)]
        public string Pincode { get; set; }
    }
}
