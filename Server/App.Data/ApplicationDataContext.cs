﻿using App.Models.Entities;
using Microsoft.EntityFrameworkCore;
using System;

namespace App.Data
{
    public class ApplicationDataContext :DbContext
    {
        public ApplicationDataContext(DbContextOptions options):base(options)  
        {

        }

        public DbSet<User> Users { get; set; }
    }
}
