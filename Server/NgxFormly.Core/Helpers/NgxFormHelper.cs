﻿using NgxFormly.Core.Attributes;
using NgxFormly.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Ngx.Core.Helpers
{
    public class NgxFormHelper
    {
        public static List<FormFieldConfiguration> GetFormFieldDefination(Type targetClass)
        {
            var dtoFormFieldDefinations = new List<FormFieldConfiguration>();
            
            foreach (var property in targetClass.GetProperties())
            {
                var formFieldConfiguration = new FormFieldConfiguration();
                var formFieldDefaultDefinations = (FormFieldDefaultDefination[])property.GetCustomAttributes(typeof(FormFieldDefaultDefination), false);
                foreach (var attr in formFieldDefaultDefinations)
                {
                    formFieldConfiguration.Key = attr.Key;
                    formFieldConfiguration.Type = attr.Type;
                }

                var formWrapperDefinations = (FormWrapperDefination[])property.GetCustomAttributes(typeof(FormWrapperDefination), false);
                foreach (var attr in formWrapperDefinations)
                {
                    formFieldConfiguration.Wrappers = new string[] { attr.Wrappers };
                }

                var formTemplateOptionsDefinations = (FormTemplateDefination[])property.GetCustomAttributes(typeof(FormTemplateDefination), false);
                foreach (var attr in formTemplateOptionsDefinations)
                {
                    formFieldConfiguration.TemplateOptions = new TemplateOptions();
                    formFieldConfiguration.TemplateOptions.Label = attr.Label;
                    formFieldConfiguration.TemplateOptions.Type = attr.Type;
                    formFieldConfiguration.TemplateOptions.Required = attr.Required;
                    formFieldConfiguration.TemplateOptions.PlaceHolder=attr.PlaceHolder;
                }


                dtoFormFieldDefinations.Add(formFieldConfiguration);
            }
          
            return dtoFormFieldDefinations;
        }
    }
}
