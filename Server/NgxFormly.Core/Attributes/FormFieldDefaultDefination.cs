﻿using NgxFormly.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NgxFormly.Core.Attributes
{
    public class FormFieldDefaultDefination :Attribute
    {
        public string Key { get; set; }

        public string Type { get; set; }
    }
}
