﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NgxFormly.Core.Attributes
{
    public class FormWrapperDefination :Attribute
    {
        public string Wrappers { get; set; }
    }
}
