﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NgxFormly.Core.Attributes
{
    public class FormTemplateDefination:Attribute
    {
        public string Label { get; set; }
        public string Type { get; set; }
        public string PlaceHolder { get; set; }
        public bool Required { get; set; }
    }
}
