﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NgxFormly.Core.Models
{
    public class FormFieldConfiguration
    {
        public string Key { get; set; }

        public string Type { get; set; }

        public string[] Wrappers { get; set; }

        public TemplateOptions TemplateOptions { get; set; }
    }
}
