﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NgxFormly.Core.Models
{
    public class TemplateOptions
    {
        public string Label { get; set; }

        public string Type { get; set; }

        public string PlaceHolder { get; set; }

        public bool Required { get; set; }
    }
}
