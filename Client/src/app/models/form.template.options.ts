export class TemplateOptions {
  label: string;
  type: string;
  placeholder: string;
  required: boolean;
}
