import { TemplateOptions } from './form.template.options';

export class FormFieldDefination {
  key: string;
  type: string;
  wrappers: string[];
  templateOptions: TemplateOptions;
}
