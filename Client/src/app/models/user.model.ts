export class User {
  userName: string;
  emailId: string;
  gender: string;
  mobileNumber: string;
  pincode: string;
}
