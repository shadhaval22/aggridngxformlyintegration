export class GridColumnDefination {
  headerName: string;
  field: string;
  editable: boolean;
  enableSorting: boolean;
  sortable: boolean;
}
