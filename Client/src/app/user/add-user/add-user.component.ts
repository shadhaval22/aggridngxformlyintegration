import { UserService } from './../../services/user.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { FormlyFieldConfig, FormlyFormOptions } from '@ngx-formly/core';
import { DefinationService } from 'src/app/services/defination.service';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.scss'],
})
export class AddUserComponent implements OnInit {
  form = new FormGroup({});
  model: any = {};
  options: FormlyFormOptions = {};
  fields: FormlyFieldConfig[];

  constructor(
    private toastr: ToastrService,
    private userService: UserService,
    private router: Router,
    private definationService: DefinationService
  ) {
    this.initializeFormFieldConfiguration();
  }

  ngOnInit(): void {}

  initializeFormFieldConfiguration() {
    this.definationService.getUserDTOFormFieldDefination().subscribe((data) => {
      this.fields = data;
    });
  }

  onSubmit() {
    if (this.form.invalid) {
      return;
    }
    this.userService.addUser(this.model).subscribe((data) => {
      this.toastr.success('success', data.toString());
      this.model = {};
      this.router.navigate(['users']);
    });
  }

  reset() {
    this.model = {};
  }

  Cancel() {
    this.router.navigate(['users']);
  }
}
