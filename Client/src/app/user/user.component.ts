import { UserService } from './../services/user.service';
import { User } from './../models/user.model';
import { Component, OnInit } from '@angular/core';
import { ColDef, ColumnApi, GridApi } from 'ag-grid-community';
import { Router } from '@angular/router';
import { DefinationService } from '../services/defination.service';
import { GridColumnDefination } from '../models/grid.column.definatinon.model';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss'],
})
export class UserComponent implements OnInit {
  // row data and column definations
  public users: User[];
  //public columnDefs: ColDef[];
  public columnDefs: GridColumnDefination[];

  // gridApi and columnApi
  private api: GridApi;
  private columnApi: ColumnApi;

  constructor(
    private userService: UserService,
    private definationService: DefinationService,
    private router: Router
  ) {
    this.definationService
      .getUserDTOGridColumnDefination()
      .subscribe((data) => {
        console.log(data);
        this.columnDefs = data;
      });
  }

  ngOnInit(): void {
    this.userService.getUsers().subscribe((data) => {
      this.users = data;
    });
  }

  // one grid intialization,grap the APIs and auto resize the columns
  onGridReady(params: any): void {
    this.api = params.api;
    this.columnApi = params.columnApi;
    this.api.sizeColumnsToFit();
  }

  // addNew User

  addNewUser() {
    this.router.navigate(['addUser']);
  }
}
