export class AppConstants {
  public static BASE_API = 'https://localhost:44334';
  public static USERS_BASE_API = '/api/Users';
  public static GET_ALL_USERS = '/GetUserDetails';
  public static INSERT_NEW_USER = '/InsertNewUser';

  public static DEFINATION_BASE_API = '/api/Defination';
  public static USERDTO_GRID_COLUMNDEFINATION = '/getUserGridColumnDefination';
  public static USERDTO_FORM_FIELDDEFINATION = '/getUserFormDefination';
}
