import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { Environment } from 'ag-grid-community';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from '../models/user.model';
import { AppConstants } from '../constants/app.constants';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  private BASE_USERS_API_ENDPOINT = `${AppConstants.BASE_API}${AppConstants.USERS_BASE_API}`;
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
  };

  constructor(private http: HttpClient) {}

  getUsers(): Observable<User[]> {
    return this.http.get<User[]>(
      `${this.BASE_USERS_API_ENDPOINT}${AppConstants.GET_ALL_USERS}`
    );
  }

  addUser(user: User): Observable<string> {
    return this.http.post<string>(
      `${this.BASE_USERS_API_ENDPOINT}${AppConstants.INSERT_NEW_USER}`,
      user,
      this.httpOptions
    );
  }
}
