import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AppConstants } from '../constants/app.constants';
import { FormFieldDefination } from '../models/form.field.defination';
import { GridColumnDefination } from '../models/grid.column.definatinon.model';

@Injectable({
  providedIn: 'root',
})
export class DefinationService {
  private BASE_DEFINATION_API_ENDPOINT = `${AppConstants.BASE_API}${AppConstants.DEFINATION_BASE_API}`;
  constructor(private http: HttpClient) {}

  getUserDTOGridColumnDefination(): Observable<GridColumnDefination[]> {
    return this.http.get<GridColumnDefination[]>(
      `${this.BASE_DEFINATION_API_ENDPOINT}${AppConstants.USERDTO_GRID_COLUMNDEFINATION}`
    );
  }

  getUserDTOFormFieldDefination(): Observable<FormFieldDefination[]> {
    return this.http.get<FormFieldDefination[]>(
      `${this.BASE_DEFINATION_API_ENDPOINT}${AppConstants.USERDTO_FORM_FIELDDEFINATION}`
    );
  }
}
